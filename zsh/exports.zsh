# vi: ft=zsh

# colorized ls
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx

# set my editor
export EDITOR='vim'

# make it easier to get to my rc files
export MYVIMRC=$HOME/.vimrc
export MYZSHRC=$HOME/.zshrc
export MYVIMKEYMAPS=$HOME/.vim/plugin/keymaps.vim
export MYVIMSETTINGS=$HOME/.vim/plugin/settings.vim

# nvm
export NVM_DIR=~/.nvm
