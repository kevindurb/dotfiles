# Dotfiles

## Installing
```
curl -o- https://bitbucket.org/kevindurb/dotfiles/raw/master/scripts/install.sh | bash
```

## Updating
```
~/dotfiles/scripts/update.sh
```
